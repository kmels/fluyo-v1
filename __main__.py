from flask import Flask, request, redirect
from twilio.twiml.messaging_response import MessagingResponse
from twilio import twiml
from twilio.rest import Client
from datetime import datetime
from datetime import timezone

twilio_account_sid = 'AC3b2e4774c7db3412bbc441ef91b17ace'
twilio_auth_token = 'a7e0405e44f719c39cbc267975276970'
twilio_client = Client(twilio_account_sid, twilio_auth_token)

app = Flask(__name__)

from pymongo import MongoClient
client = MongoClient('localhost',27018)
db = client.fluyodev

import time
import logging

ENGLISH_READY = "english-ready"
ONBOARD_UNSTARTED = "onboard-unstarted"
NEEDS_REGISTRATION = "needs-registration"
SPANISH_READY = "spanish-ready"
NEEDS_JOIN = "needs-join"
NEEDS_JOIN_FIX = "needs-join-fix"
JOIN_READY = "join-ready"
NEEDS_MENU = "needs-menu"
NEEDS_BALANCE = "needs-balance"
BALANCE_READY = "balance-ready"
NEEDS_REQUEST = "needs-request"
NEEDS_REQUEST_FIX = "needs-request-fix"
REQUEST_READY = "request-ready"
NEEDS_REQUEST_NEEDS_REGISTRATION = "needs-request-needs-registration"
NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION = "needs-request-needs-creditor-confirmation"
NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION_FIX = "needs-request-needs-creditor-confirmation-fix"
NEEDS_REQUEST_CREDITOR_DENIED = "needs-request-creditor-denied"
NEEDS_REQUEST_NEEDS_TRANSACTION = "needs-request-needs-transaction"
NEEDS_REQUEST_NEEDS_WITHDRAWAL = "needs-request-needs-withdrawal"

NEEDS_SEND = "needs-send"
NEEDS_SEND_FIX = "needs-send-fix"
SEND_READY = "send-ready"
SEND_READY_NEEDS_BALANCE = "send-ready-needs-balance"
SEND_READY_NEEDS_REGISTRATION = "send-ready-needs-registration"
SEND_READY_NEEDS_JOIN = "send-ready-needs-join"
SEND_READY_JOIN_READY = "send-ready-join-ready"
SEND_READY_NEEDS_WITHDRAWAL = "send-ready-needs-withdrawal"
SEND_READY_WITHDRAWAL_READY = "send-ready-withdrawal-ready"
SEND_READY_WITHDRAWAL_SENT = "send-ready-withdrawal-ready"

NEEDS_INVITE = "needs-invite"
NEEDS_INVITE_FIX = "needs-invite-fix"
INVITE_READY = "invite-ready"
INVITE_READY_JOIN_READY = "invite-ready-join-ready"
INVITE_READY_NEEDS_REGISTRATION = "invite-ready-needs-registration"
INVITE_READY_NEEDS_REGISTRATION_FIX = "invite-ready-needs-registration-fix"
INVITE_READY_NEEDS_JOIN = "invite-ready-needs-join"
INVITE_READY_REGISTERED_READY = "invite-ready-registered-ready"
INVITE_READY_JOIN_READY = "invite-ready-join-ready"
INVITE_DENIED = "invite-denied"
NEEDS_INFO = "needs-info"
SAY_THANKS = "say-thanks"

expected_actions = {
    ('*','hello'): ENGLISH_READY,
    ('*','hola'): SPANISH_READY,
    (ONBOARD_UNSTARTED,'hello'): ENGLISH_READY,
    (ONBOARD_UNSTARTED,'hola'): SPANISH_READY,
    ('*', '1'): NEEDS_REGISTRATION,
    ('*', '2'): NEEDS_BALANCE,
    ('*', '3'): NEEDS_SEND,
    ('*', '4'): NEEDS_REQUEST,
    ('*', '5'): NEEDS_INVITE,
    (SEND_READY_WITHDRAWAL_READY, '6'): SAY_THANKS,
    ('*', 'menu'): NEEDS_MENU,
    (SEND_READY_NEEDS_REGISTRATION, '*'): NEEDS_MENU,
    (NEEDS_REQUEST_NEEDS_REGISTRATION, '*'): NEEDS_MENU,
    (NEEDS_REQUEST_NEEDS_TRANSACTION, '*'): NEEDS_MENU,
    (NEEDS_REQUEST_CREDITOR_DENIED, '*'): NEEDS_MENU,
    (INVITE_READY_JOIN_READY, '*'): NEEDS_MENU,

}

replies = {
    (ONBOARD_UNSTARTED, "en"): """'hello' for English, 'hola' for Spanish""",
    (ENGLISH_READY, "en"): """1 to join, 2 to check balance, 3 to send, 4 to request, 5 to invite, 'info' for information""",
    (SPANISH_READY, "es"): """1 para unirte, 2 para chequear saldo, 3 para enviar, 4 para solicitar, 5 para invitar, 'info' para más información""",
    (NEEDS_REGISTRATION, "en"): "Awesome! Type your name and number in this format: name, phone number. EX: Jorge, 3109181918",
    (NEEDS_REGISTRATION, "es"): "Excelente, te añado en cuanto me des tu nombre y teléfono en este formato: nombre, teléfono completo. EJ: Jorge, 50250501212",
    (JOIN_READY, "en"): lambda meta: "Awesome, added! You now have %s magi tokens. What would you like to do? ('menu' for list of instructions." % (meta['magi'] if meta != None and meta.get('magi',0) > 0 else 'no'),
    (JOIN_READY, "es"): lambda meta: "Genial, añadido! Ahora tienes %s tokens magi. Qué quieres hacer? ('menu' para lista de instrucciones." % (meta['magi'] if meta != None and meta.get('magi',0) > 0 else 'ningún'),
    (NEEDS_JOIN_FIX, "en"): "Oops, sorry, the formatting isn't quite right. Make sure it is: name, number. EX: Jorge, 3105551212",
    (NEEDS_JOIN_FIX, "es"): "Oops, lo siento, el formato no está bien. Asegura que sea: nombre, número. EJ: Jorge, 50230359775",
    (NEEDS_MENU, "en"): """1 to join, 2 to check balance, 3 to send, 4 to request, 5 to invite, 'info' for information""",
    (NEEDS_MENU, "es"): """1 para unirte, 2 para chequear saldo, 3 para enviar, 4 para solicitar, 5 para invitar, 'info' para más información""",
    (NEEDS_REQUEST, "en"): """Tell us the amount and the number of the friend you’re asking in this format: -amount, full number. EX.: -2, 3105551212 Or reply with ‘info’""",
    (NEEDS_REQUEST, "es"): """Entra el monto y el número de tu beneficiario en este formato : -cantidad, teléfono completo. EJ.: -2, 3105551212 O responde con 'info'""",
    (NEEDS_REQUEST_FIX, "en"): "Oops, sorry, the formatting isn't quite right. Make sure it is: amount, number. EX: +5, 3105551212",
    (NEEDS_REQUEST_FIX, "es"): "Oops, lo siento, el formato no está bien. Asegura que sea: cantidad, número. EJ: +5, 50230359775",
    (BALANCE_READY, "en"): lambda meta: ("""Your balance is: %.2f magi tokens and %d points""" % (meta['magi'], meta['points'])) if meta != None and (meta.get('magi',0) > 0 or meta.get('points', 0) > 0) else """sorry, no tokens, no points. """,
    (BALANCE_READY, "es"): lambda meta: ("""Tu balance es: %.2f magi tokens y %d puntos""" % (meta['magi'], meta['points'])) if meta != None and (meta.get('magi',0) > 0 or meta.get('points', 0) > 0) else """Lo siento, sin tokens, sin puntos. """,
    (NEEDS_SEND, "en"): "Tell us the amount and your beneficiary’s number in this format: +amount, number. EX.: +5, 13105551212 Or reply with ‘info’",
    (NEEDS_SEND, "es"): "Entra el monto y el número de tu beneficiario en este formato : cantidad, teléfono completo. EJ: +5, 50250501212",
    (NEEDS_SEND_FIX, "en"): "Oops, sorry, the formatting isn't quite right. Make sure it is: amount, number. EX: +5, 3105551212",
    (NEEDS_SEND_FIX, "es"): "Oops, lo siento, el formato no está bien. Asegura que sea: cantidad, número. EJ: +5, 50230359775",
    (SEND_READY_NEEDS_BALANCE, "en"): "Not enough tokens. (2 to check balance)",
    (SEND_READY_NEEDS_BALANCE, "es"): "No hay suficientes tokens. (2 para revisar balance)",
    (SEND_READY_NEEDS_REGISTRATION, "en"): "Your friend was not registered and was just invited",
    (SEND_READY_NEEDS_REGISTRATION, "es"): "Your friend was not registered and was just invited",
    (SEND_READY_WITHDRAWAL_READY, "en"): "Your friend sent you 5 tokens! (2 for balance, 6 to say thanks)",
    (SEND_READY_WITHDRAWAL_READY, "es"): "Your friend sent you 5 tokens! (2 for balance, 6 to say thanks)",
    (SEND_READY_WITHDRAWAL_SENT, "en"): "Your friend thanks you for the %.2f tokens and you earned 10 points!",
    (SEND_READY_WITHDRAWAL_SENT, "es"): "Tu amigo te agradece por los %.2f tokens y te ganaste 10 puntos!",
    (NEEDS_REQUEST_NEEDS_REGISTRATION, "en"): "Your friend is not registered yet, invite them first.",
    (NEEDS_REQUEST_NEEDS_REGISTRATION, "es"): "Tu amigo no está registrado aún, invitalo primero.",
    (NEEDS_REQUEST_NEEDS_TRANSACTION, "en"): "Your friend has been asked to accept your request",
    (NEEDS_REQUEST_NEEDS_TRANSACTION, "es"): "Le preguntamos a tu amigo si quiere aceptar tu petición",
    (NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION_FIX, "en"): "Oops, sorry, the formatting isn't quite right. Answer 'Yes' or 'No'",
    (NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION_FIX, "es"): "Oops, lo siento, el formato no está bien. Responde 'Si' o 'No'",
    (NEEDS_REQUEST_CREDITOR_DENIED, "en"): "Sorry, your friend denied your request.",
    (NEEDS_REQUEST_CREDITOR_DENIED, "es"): "Lo sentimos, tu amigo denegó tu petición.",
    (NEEDS_INVITE, "en"): "Tell us who you’re inviting in this format: inv full number. EX.: inv 13105551212",
    (NEEDS_INVITE, "es"): "Dinos a quien quieres invitar en este formato: inv número completo. EJ.: inv 50230359775",
    (NEEDS_INVITE_FIX, "en"): "Oops, sorry, the formatting isn't quite right. Make sure it is: inv full number. EX: inv 3105551212",
    (NEEDS_INVITE_FIX, "es"): "Oops, lo siento, el formato no está bien. Asegura que sea: inv número completo. EJ: inv 50230359775",
    (INVITE_READY_JOIN_READY, "en"): "Your friend is already registered! try sending them tokens.",
    (INVITE_READY_JOIN_READY, "es"): "Tu amigo ya esta registrado! Intenta enviarle tokens.",
    (INVITE_READY_NEEDS_JOIN, "en"): "Your friend was not registered and was just invited",
    (INVITE_READY_NEEDS_JOIN, "es"): "Tu amigo no estaba registrado y ya fue invitado",
    (INVITE_READY_NEEDS_REGISTRATION_FIX, "en"): "Oops, sorry, the formatting isn't quite right. Answer 'Yes' or 'No'",
    (INVITE_READY_NEEDS_REGISTRATION_FIX, "es"): "Oops, lo siento, el formato no está bien. Responde 'Si' o 'No'",
    (INVITE_READY_REGISTERED_READY,"en"): "Your friend accepted your invitation! You won 50 points.",
    (INVITE_READY_REGISTERED_READY, "es"): "Tu amigo aceptó tu invitación! Ganaste 50 puntos.",
    (INVITE_READY_JOIN_READY, "en"): lambda meta: "Welcome! You have %.2f magi tokens. What would you like to do next? ('menu' para lista de instrucciones)",
    (INVITE_READY_JOIN_READY, "es"): lambda meta: "Bienvenido! Ahora tienes .%2f tokens magi. Qué quieres hacer? ('menu' para lista de instrucciones). ",
    (NEEDS_INFO, "en"): "For help and information, go to http://fluyo.money/eng",
    (NEEDS_INFO, "es"): "Para ayuda y más información, ve a http://fluyo.money/esp",
    (INVITE_DENIED, "en"): "bummer",
    (INVITE_DENIED, "es"): "lástima"
}

### Initialize collections, create if missing
BALANCE_COLLECTION = "balance"
LANGUAGE_COLLECTION = "language"
DEPOSIT_COLLECTION = 'deposit'
INVITE_COLLECTION = "invite"
NEEDS_REGISTRATION_COLLECTION = "needs-registration"
RECV_COLLECTION = 'recv'
REQUEST_COLLECTION = 'request'
SENT_COLLECTION = 'sent'
TRANSACTION_COLLECTION = "transaction"
USER_STATE_COLLECTION = 'user_state'
USERS_COLLECTION = 'users'

collections = [BALANCE_COLLECTION, LANGUAGE_COLLECTION, NEEDS_REGISTRATION_COLLECTION, RECV_COLLECTION, USER_STATE_COLLECTION, USERS_COLLECTION]
cols = db.list_collection_names()

for col in [c for c in collections if not c in cols]:
    print("Creating collection %s ... " % col)
    db.create_collection(col)

def parse_phone(t, prefix):
    if ':' in t:
        t = t[(t.index(':')+1):-1]
    digits = t.replace("-","").replace("+","").strip()
    if len(digits) == 8:
        return "+502"+str(int(digits))
    else:
        return "+"+str(int(digits))

## For actions that need parsing and validation
def transition_to_next_state(current_state, number, action, prefix):
    print("\ttransitioning to next state ...")
    next_state = current_state
    next_state_meta = {}

    if current_state in [NEEDS_REGISTRATION, NEEDS_JOIN_FIX]:

        needs_friendly_message = get_sent(number, 'body', '') != replies[(NEEDS_REGISTRATION, get_language(number))]

        if needs_friendly_message and current_state == NEEDS_REGISTRATION:
            print("Needs friendly message")
            return (next_state, next_state_meta)

        success = False
        # check valid format
        parts = action.split(',')
        if len(parts) == 2:
            try:
                # PARSE: <name>, <phone>
                name = parts[0]
                phone = parse_phone(parts[1], prefix)
                (next_state, next_state_meta) = (NEEDS_JOIN, {"name": name, "number": phone})
                success = True
            except Exception as e:
                print(str(e))
                pass
        if not success:
            (next_state, next_state_meta) = (NEEDS_JOIN_FIX, {})

    elif current_state == NEEDS_BALANCE:
        user_balance = get_collection_value(BALANCE_COLLECTION, number)
        (next_state, next_state_meta) = (BALANCE_READY, user_balance)

    ###
    # 3. Validate when user chooses to send tokens
    # If he didn't have enough balance before, we should keep accepting send
    # requests
    ###
    elif current_state in [NEEDS_SEND, NEEDS_SEND_FIX, SEND_READY_NEEDS_BALANCE]:
        success = False
        parts = action.split(",")
        if len(parts) == 2:
            try:
                # PARSE: <amount>, <phone>
                amount = float(parts[0].replace("+",""))
                number = parse_phone(parts[1], prefix)
                (next_state, next_state_meta) = (SEND_READY, {"amount": amount, "number": number})
                print(str(next_state_meta))
                success = True
            except Exception as e:
                print(str(e))
                pass
        if not success:
            (next_state, next_state_meta) = (NEEDS_SEND_FIX, {})
    elif current_state in [NEEDS_REQUEST, NEEDS_REQUEST_FIX]:
        success = False
        parts = action.split(",")
        if len(parts) == 2:
            try:
                # PARSE: <amount>, <phone>
                amount = float(parts[0].replace("-",""))
                number = parse_phone(parts[1], prefix)
                (next_state, next_state_meta) = (REQUEST_READY, {"amount": amount, "number": number})
                print(str(next_state_meta))
                success = True
            except Exception as e:
                print(str(e))
                pass
        if not success:
            (next_state, next_state_meta) = (NEEDS_SEND_FIX, {})
    elif current_state in [NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION, NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION_FIX]:
        # pass only if answwer is Yes
        success = False
        if action.upper() == "NO":
            success = True
            next_state = NEEDS_REQUEST_CREDITOR_DENIED
        elif action.upper() == "YES":
            success = True
            next_state = NEEDS_REQUEST_NEEDS_WITHDRAWAL
        if not success:
            (next_state, next_state_meta) = (NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION_FIX, {})
    elif current_state in [NEEDS_INVITE, NEEDS_INVITE_FIX]:
        success = False
        parts = action.replace(","," ").strip().split(" ")
        if len(parts) == 2:
            try:
                invitee_number = parse_phone(parts[1], prefix)
                if parts[0].lower() == 'inv':
                    (next_state, next_state_meta) = (INVITE_READY, {"invitee_phone": invitee_number, "inviter": number})
                    success = True
            except Exception as e:
                print(str(e))
                pass
        if not success:
            (next_state, next_state_meta) = (NEEDS_INVITE_FIX, {})

    elif current_state in [INVITE_READY_NEEDS_REGISTRATION, INVITE_READY_NEEDS_REGISTRATION_FIX]:

        # pass only if answwer is Yes
        success = False
        if action.upper() == "NO":
            success = True
            next_state = INVITE_DENIED
        elif action.upper() == "YES":
            success = True
            next_state = NEEDS_REGISTRATION
        if not success:
            (next_state, next_state_meta) = (INVITE_READY_NEEDS_REGISTRATION_FIX, {})

    print("Next (transitioned) state: ", next_state)
    print("Next (transitioned) state meta: ", next_state_meta)

    return (next_state, next_state_meta)

def remove_collection_value(collection, number):
    if number == None:
        return
    if number is dict:
        return db.get_collection(collection).delete_one(number)
    else:
        return db.get_collection(collection).delete_one({"number": number})

def get_collection_value(collection, number, key = None, fallback_value = None):
    ret = fallback_value

    if type(number) is dict:
        print("Finding one dict: ", number)
        payload = db.get_collection(collection).find_one(number)
    else:
        print("Finding one number: ", number)
        payload = db.get_collection(collection).find_one({"number": number})

    if payload != None:
        if key != None:
            ret = payload['data'].get(key, fallback_value)
        else:
            ret = payload['data']
    else:
        print("Warning: ", str(type(number)), "query did not match any document. Query: ", number)
    ret_str = str(ret) if ret != None else "None (payload: %s)" % payload
    print("Getting %s/%s%s => %s" % (collection, number, "" if key == None else "@"+key, ret_str))
    return ret

def get_state(number, field = 'state'):
    return get_collection_value(USER_STATE_COLLECTION, number, field, ONBOARD_UNSTARTED)

def get_recv(number, field = 'date', fallback_value = None):
    return get_collection_value(RECV_COLLECTION, number, field, fallback_value)

def get_sent(number, field = 'date', fallback_value = None):
    return get_collection_value(SENT_COLLECTION, number, field, fallback_value)

def get_user(number):
    return get_collection_value(USERS_COLLECTION, number)

def save_recv(number, body):
    save_db(RECV_COLLECTION, number, {"body": body, "date": datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()})

def save_sent(number, body):
    save_db(SENT_COLLECTION, number, {"body": body, "date": datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()})

def save_state(number, state):
    if state == None:
        return
    save_db(USER_STATE_COLLECTION, number, {"state": state, "date": datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()})

def save_db(collection, number, data):
    doc = {"number": number, "data": data}
    existing = db.get_collection(collection).find_one({"number": number})
    if existing == None:
        db.get_collection(collection).insert_one(doc)
    else:
        db.get_collection(collection).update_one({'number': number}, {"$set": {"data": data}})

def get_language(number):
    return get_collection_value(LANGUAGE_COLLECTION, number, 'lang', 'en')

def set_language(number, lang):
    save_db(LANGUAGE_COLLECTION, number, {"lang": lang})

def send_out(prefix, recipient_phone, message_body, template_body = None):

    if recipient_phone == None:
        print("ERROR: Recipient phone cannot be None in send_out function.")
        raise ValueError("ERROR: Recipient phone cannot be None in send_out function.")

    print("Messaging unregistered recipient ... ")
    dt = get_recv(recipient_phone, 'date')
    recipients_first_message = dt is None

    if dt != None:
        print("Last recv: ", dt)
        dt = datetime.utcfromtimestamp(dt)
        print("Date: ", dt)
        elapsed = (datetime.utcnow() - dt).total_seconds()
        print("Seconds elapsed since response: ", elapsed)

        one_day_of_seconds = 24*3600
        has_chatted_in_24hwindow = one_day_of_seconds > elapsed
        recipients_first_message = not has_chatted_in_24hwindow

    if not recipients_first_message:
        if prefix == 'whatsapp':

            print("Sending out whatsapp message to %s with message body = %s" % (recipient_phone, template_body))

            message = twilio_client.messages.create(
                          body=message_body,
                          from_='whatsapp:+14155238886',
                          to='whatsapp:'+recipient_phone
                      )
            print(str(message))
            print(message.sid)

        else:
            print("TODO prefix: "+prefix)
        return JOIN_READY
    else:
        if prefix == 'whatsapp' and template_body != None:

            print("Sending out whatsapp message to %s with template body = %s" % (recipient_phone, template_body))

            message = twilio_client.messages.create(
                          body=template_body,
                          from_='whatsapp:+14155238886',
                          to='whatsapp:'+recipient_phone
                      )
            print(str(message))
            print(message.sid)
        else:
            print("TODO prefix: "+prefix)
        return NEEDS_REGISTRATION

def make_transfer(number, recipient_phone, amount, points):
    print("%s transfering to %s: %s magi tokens, %s points" % (number, recipient_phone, amount, points))
    # recipient is registered
    sender_balance = get_collection_value(BALANCE_COLLECTION, number)
    recipient_balance = get_collection_value(BALANCE_COLLECTION, recipient_phone)

    sender_balance['magi'] -= amount
    recipient_balance['magi'] += amount
    sender_balance['points'] += points

    save_db(BALANCE_COLLECTION, recipient_phone, recipient_balance)
    save_db(BALANCE_COLLECTION, number, sender_balance)

    tx = {'recipient': recipient_phone, 'amount': amount, 'points': points}
    tx['recipient_balance'] = recipient_balance
    tx['sender_balance'] =  sender_balance
    save_db(TRANSACTION_COLLECTION, number, tx)
    #send_out(prefix, recipient_phone, message_body)

    remove_collection_value(DEPOSIT_COLLECTION, {"number": recipient_phone, "data.creditor": number})
    # remove deposit..

def recv_in(body, number, prefix):
    resp = MessagingResponse()
    current_state = get_state(number)

    user = get_user(number)
    is_registered = user != None

    # Get the transition given the current state and action pair
    action = body
    next_state = expected_actions.get((current_state, action.lower()))
    next_state_meta = {}

    if next_state == None:
        next_state = expected_actions.get(('*', action.lower()))
    if next_state == None:
        next_state = expected_actions.get((current_state, '*'))

    if next_state == None:
        (next_state, next_state_meta) = transition_to_next_state(current_state, number, action.lower(), prefix)
        print("Next state meta: ", next_state_meta)

    print("State + Action => ", next_state)
    if next_state_meta != {}:
        print("Next state meta: ", next_state_meta)

    if next_state != None:
        save_state(number, next_state)

    if next_state == ENGLISH_READY:
        set_language(number, 'en')
    elif next_state == SPANISH_READY:
        set_language(number, 'es')
    elif next_state == NEEDS_REGISTRATION:
        save_db(NEEDS_REGISTRATION_COLLECTION, number, {'timestamp': time.time()})
    elif next_state == NEEDS_JOIN:
        is_registered = get_collection_value(USERS_COLLECTION, number) != None
        if not is_registered:
            save_db(USERS_COLLECTION, number, next_state_meta)

        next_state = JOIN_READY
        save_state(number, JOIN_READY)

        if not is_registered:
            new_user_balance =  {"magi": 10, 'points': 0}
            save_db(BALANCE_COLLECTION, number, new_user_balance)

        incoming_magi = get_collection_value(DEPOSIT_COLLECTION, number, 'amount', 0)
        print("Incoming magi: ", incoming_magi)

        if incoming_magi > 0:
            deposit = get_collection_value(DEPOSIT_COLLECTION, number)
            print("Pending deposit: ", deposit)
            next_state_meta.update(deposit)
            #next_state_meta['creditor'] = get_collection_value(DEPOSIT_COLLECTION, number, 'creditor')

            print("Messaging creditor: ", next_state_meta['creditor'])
            make_transfer(next_state_meta['creditor'], number, next_state_meta['amount'], 10)

            send_out(prefix, next_state_meta['creditor'], "Your friend thanks you for the %d tokens, and you earned 10 points!" % incoming_magi)

            # next_state_meta['counterpary_next_state'] = SEND_READY_JOIN_READY
            save_state(deposit['creditor'], SEND_READY_JOIN_READY)

        find_invite = {"data.invitee_phone": number}
        filling_invite = get_collection_value(INVITE_COLLECTION, find_invite)

        print("Filling invite: ", filling_invite)
        if filling_invite != None:
            inviter = filling_invite['inviter']

            print("Rewarding inviter: ", inviter)
            inviter_balance = get_collection_value(BALANCE_COLLECTION, inviter)
            inviter_balance['points'] += 50
            save_db(BALANCE_COLLECTION, inviter, inviter_balance)

            message_body = replies[INVITE_READY_REGISTERED_READY, get_language(inviter)]
            template_body = 'Your friend accepted your invitation! You won 50 points. Invitee\'s code is %s' % (number)

            send_out(prefix, inviter, message_body, template_body)
            remove_collection_value(INVITE_COLLECTION, find_invite)

        next_state_meta["magi"] = get_collection_value(BALANCE_COLLECTION, number, 'magi', 0)
        next_state_meta["points"] = get_collection_value(BALANCE_COLLECTION, number, 'points', 0)

    elif next_state == NEEDS_JOIN_FIX:
        print("Warning..")
        pass
    elif next_state == NEEDS_BALANCE:
        (next_state, next_state_meta) = transition_to_next_state(next_state, number, action.lower(), prefix)
        save_state(number, next_state)
    elif next_state == REQUEST_READY:

        # creditor is registered?
        creditor_phone = next_state_meta['number']
        creditor = get_collection_value(USERS_COLLECTION, creditor_phone, "number")
        creditor_is_registered = not creditor is None
        amount = next_state_meta['amount']
        next_state_meta['requester'] = number

        if creditor_is_registered:
            message_body = "Your friend is requesting %.2f magi from you. Type Yes to accept." % amount
            template_body = 'Your friend just requested %.2f magi tokens from you. Type Yes to accept. Your recipients\'s code is %s' % (amount, number)

            print("Creditor: ", creditor)

            send_out(prefix, creditor_phone, message_body, template_body)

            save_state(creditor_phone, NEEDS_REQUEST_NEEDS_CREDITOR_CONFIRMATION)
            next_state = NEEDS_REQUEST_NEEDS_TRANSACTION

            save_db(REQUEST_COLLECTION, number, next_state_meta)
        else:
            next_state = NEEDS_REQUEST_NEEDS_REGISTRATION
    elif next_state == SEND_READY:
        # Check balance:
        magis = get_collection_value(BALANCE_COLLECTION, number, 'magi', 0)

        amount = next_state_meta['amount']
        recipient_phone = next_state_meta['number']

        has_enough_balance = amount <= magis
        if not has_enough_balance:
            next_state = SEND_READY_NEEDS_BALANCE
        else:
            recipient = get_collection_value(USERS_COLLECTION, recipient_phone, "number")
            print("Recipient: ", recipient)
            recipient_is_registered = not recipient is None

            if not recipient_is_registered:
                # 7.1 "Sent ready and needs registration" -
                message_body = "Your friend %s sent you tokens. Type 1 to accept." % number
                template_body = 'Your balance just changed by +%.2f magi tokens. Your sender\'s code is %s' % (amount, number)

                next_state = SEND_READY_NEEDS_REGISTRATION
                next_state_meta['timestamp'] = time.time()

                # Log pair (phone, timestamp) to send_ready_unregistered(phone, timestamp)
                save_db(NEEDS_REGISTRATION, recipient_phone, next_state_meta)

                next_state_meta['creditor'] = number
                save_db(DEPOSIT_COLLECTION, recipient_phone, next_state_meta)

                out_state = send_out(prefix, recipient_phone, message_body, template_body=template_body)
                # Check if the user was registered already and allow to answer with 1 next
                if out_state == JOIN_READY:
                    save_db(USER_STATE_COLLECTION, recipient_phone, {"state": NEEDS_MENU})
                else:
                    save_db(USER_STATE_COLLECTION, recipient_phone, {"state": NEEDS_REGISTRATION})
            else:
                make_transfer(number, recipient_phone, amount, 10)
                next_state = SEND_READY_WITHDRAWAL_SENT

                message_body = "Your friend sent you %.2f tokens! (2 for balance, 6 to say thanks)." % amount
                template_body = 'Your balance just changed by +%.2f magi tokens. Your sender\'s code is %s' % (amount, number)
                send_out(prefix, recipient_phone, message_body, template_body)
                save_state(recipient_phone, SEND_READY_WITHDRAWAL_READY)

    elif next_state == SEND_READY_NEEDS_WITHDRAWAL:
        withdrawal_attempts = get_collection_value(WITHDRAWAL_COLLECTION, {"number", number}, 'recipients', [])

        if withdrawal_attempts == []:
            print("THE IMPOSSIBLE HAPPENED. CANNOT FIND WITHDRAWAL.")
        else:
            withdrawal = withdrawal_attempts

        recipient_balance = get_collection_value(BALANCE_COLLECTION, recipient_phone)
        sender_balance = get_collection_value(BALANCE_COLLECTION, number)
    elif next_state == NEEDS_REQUEST_CREDITOR_DENIED:
        find_requestor = {"data.number": number}
        requester_phone = get_collection_value(REQUEST_COLLECTION, find_requestor, 'requester')

        if requester_phone != None:
            message_body = replies.get((next_state, get_language(number)))
            send_out(prefix, requester_phone, message_body)
            save_state(requester_phone, NEEDS_REQUEST_CREDITOR_DENIED)
            remove_collection_value(REQUEST_COLLECTION, {"data.requester": requester_phone, "data.number": number})

        next_state = NEEDS_MENU
    elif next_state == NEEDS_REQUEST_NEEDS_WITHDRAWAL:
        find_requestor = {"data.number": number}
        withdrawal_attempts = get_collection_value(REQUEST_COLLECTION, {"number", number}, 'recipients', [])

        if withdrawal_attempts == []:
            print("THE IMPOSSIBLE HAPPENED. CANNOT FIND WITHDRAWAL.")
        else:
            withdrawal = withdrawal_attempts

        recipient_balance = get_collection_value(BALANCE_COLLECTION, recipient_phone)
        sender_balance = get_collection_value(BALANCE_COLLECTION, number)
    elif next_state == INVITE_READY:
        invitee_phone = next_state_meta['invitee_phone']
        invitee = get_collection_value(USERS_COLLECTION, invitee_phone, "number")
        invitee_is_registered = not invitee is None

        if invitee_is_registered:
            next_state = INVITE_READY_JOIN_READY
        else:
            save_db(INVITE_COLLECTION, number, next_state_meta)
            message_body = "Your friend invited you to join Fluyo. Type Yes to accept."
            template_body = 'Your friend invited you to join Fluyo. Type Yes to accept. Your inviter\'s code is %s' % (number)
            send_out(prefix, invitee_phone, message_body, template_body)

            save_state(invitee_phone, INVITE_READY_NEEDS_REGISTRATION)
            next_state = INVITE_READY_NEEDS_JOIN


    save_state(number, next_state)

    language = get_language(number)
    print("State after reply: " + next_state)

    reply = replies.get((next_state, language))
    if callable(reply):
        reply = reply(next_state_meta)

    if reply != None:
        resp.message(reply)
        save_sent(number, reply)
    else:
        resp.message("Unknown state reply: "+ str((next_state,language)))
    return str(resp)
@app.route("/sms", methods=['GET', 'POST'])
def sms_reply():
    body = request.values.get('Body', None)
    print('------------- BEGIN REQUEST ----------------')
    print("Action: ", str(body))
    number = request.form['From']
    recv = recv_in(body, number, 'sms')
    save_recv(number, body)
    print('------------- END REQUEST ------------------')

@app.route("/whatsapp", methods=['GET', 'POST'])
def whatsapp_reply():
    """Respond to incoming calls with a simple text message."""
    body = request.values.get('Body', None)
    print('------------- BEGIN REQUEST ----------------')
    print("Action: ", str(body))
    number = request.form['From'].replace("whatsapp:","")
    recv = recv_in(body, number, 'whatsapp')
    save_recv(number, body)
    print('------------- END REQUEST ------------------')
    return recv
if __name__ == "__main__":
    app.run(debug=True)
