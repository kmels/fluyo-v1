from flask import Flask, request, redirect
from twilio.twiml.messaging_response import MessagingResponse
from twilio import twiml

app = Flask(__name__)

@app.route("/sms", methods=['GET', 'POST'])
def sms_reply():
    """Respond to incoming calls with a simple text message."""
    body = request.values.get('Body', None)
    resp = MessagingResponse()

    print("Body: " + body)
    # Determine the right reply for this message
    if body == 'hello':
        resp.message("Hi!")
    elif body == 'bye':
        resp.message("Goodbye")

    print(str(resp))

    number = request.form['From']
    message_body = request.form['Body']

    #resp = twiml.Response()
    resp.message('Hello {}, you said: {}'.format(number, message_body))
    print(str(resp))
    return str(resp)
    #return str(resp)

if __name__ == "__main__":
    app.run(debug=True)
